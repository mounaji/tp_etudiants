
module.exports = function(grunt) {

  grunt.initConfig({

pkg: grunt.file.readJSON('package.json'),

   
   uglify: {
      
      dist: {
        src: ['js/*.js'],
        dest: 'dist/built.js.min'
      }
    },
  watch: {
      scripts: {
        files: '**/*.js', // tous les fichiers JavaScript de n'importe quel dossier
        tasks: ['uglify:dist'],options:{livereload:true}
      }
  },
});

 
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-uglify');

  
  grunt.registerTask('dist', ['uglify:dist']); 

  grunt.registerTask('default', ['dist']);
};
