QUnit.test( "hello test", function( assert ) {
  assert.ok( 1 == "1", "Passed!" );
});
QUnit.test( "Pair1", function( assert ) {
  assert.ok(  NbPair(2), "true" );
});
QUnit.test( "Pair2", function( assert ) {
  assert.notOk(  NbPair(3), "true" );
});
QUnit.test( "Pair3", function( assert ) {
  assert.ok(  NbPair(4), "true" );
});

QUnit.test( "assertTest", function( assert ) {
  assert.expect( 6 );

  assert.ok(  NbPair(4), "true" );
  assert.notOk(  NbPair(3), "true" );
  assert.ok(  NbPair(2), "true" );
  assert.notOk(  NbPair(5), "true" );
  assert.ok(  NbPair(0), "true" );
  assert.ok(  NbPair(10), "true" );
 
  });

