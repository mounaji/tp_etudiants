<?php
use PHPUnit\Framework\TestCase;

  require_once '../src/Poneys.php';

  class MyClass extends \PHPUnit_Framework_TestCase {

  private $Poneys;

  
    public function test_removePoneyFromField() {
      //Setup
    
      // Action
     
     try {
        $this->Poneys->removePoneyFromField(3);
        
      } catch (Exception $e) {
        echo 'Exception recue :',$e->getMessage(), "\n" ;
      }
      
      // Assert
      $this->assertEquals(5,  $this->Poneys->getCount());
    }
}
