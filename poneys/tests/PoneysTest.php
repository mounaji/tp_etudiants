<?php
use PHPUnit\Framework\TestCase;
	
  require_once 'src/Poneys.php';

  class PoneysTest extends \PHPUnit_Framework_TestCase {

  private $Poneys;

    public function setUp(){
       $this->Poneys = new Poneys();
       //$this->$Poneys->setCount(10);
    }

    public function tearDown(){
        unset($this->Poneys);
    }
    public function test_removePoneyFromField() {
      // Setup
    
      // Action
     
     try {
        $this->Poneys->removePoneyFromField(3);
        
      } catch (Exception $e) {
        echo 'Exception recue :',$e->getMessage(), "\n" ;
      }
      
      // Assert
      $this->assertEquals(5,  $this->Poneys->getCount());
    }

/**
 * @dataProvider testProvider
 */
    public function testRemove($a,$b,$e){
      $this->assertEquals($e,$a-$b);
    }


    public  function testProvider(){

       // Setup
      $this->Poneys = new Poneys();

       //Action
      $this->Poneys->removePoneyFromField(3);
     // $Poneys->getCount()

      return [
              'calcul ok'=>[ $this->Poneys->getInitial(),3, $this->Poneys->getCount()],
            //  'calcul ok, nbr negatif'=>[2,3,$Poneys->getCount()]
             ];
    }

       public function testMock()
      {
          // Create a stub for the Poneys class.
          $mock = $this->getMockBuilder(Poneys::class)->setMethods(['getNames'])->getMock();
 
          // Configure the stub.
         $mock->expects($this->once())
                 ->method('getNames')
                 ->willReturn(['toto', 'titi', 'tata']);

          $this->assertEquals(['toto', 'titi', 'tata'], $mock->getNames());
      }

      public function isThereMorePlaces() {


      }
  
	  
    
    
}

	


  
 ?>
